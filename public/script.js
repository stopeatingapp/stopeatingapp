const btns = document.querySelectorAll(".img-container");
const floating = document.querySelector(".floating");
let sum = document.querySelector(".counter");
let main = document.querySelector("main");

let foodCalories = [["pizza", "ham", "ice-cream", "sushi"], [295, 200, 207, 75]];

btns.forEach(function (btn) {
    btn.addEventListener("click", function () {
        let div = btn.querySelector(".food").classList;
        let selected = div.contains("selected");
        div.toggle("selected");
        count(selected, div[1]);
    });
});

floating.addEventListener("click", () => {
    let btn = floating.querySelector("i").classList;

    main.classList.toggle("dark");
        
    btn.toggle("fa-moon");
    btn.toggle("fa-sun");

});

function count(action, food) {
    let index = foodCalories[0].indexOf(food);
    if (action === false) {
        sum.innerText = parseInt(sum.innerText) + foodCalories[1][index];
    } else {
        sum.innerText = parseInt(sum.innerText) - foodCalories[1][index];
    }
}